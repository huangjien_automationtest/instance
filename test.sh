if [ -z "$1" ]
then
    echo "Usage: test.sh <listening port number>"
    exit 0
fi
curl -X POST --header "Content-Type:application/json" -d @testData.json http://localhost:$1/test
