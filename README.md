# Automation-Test Client Instance

Deploy a docker to node, accept json format commands to run end to end selenium test (headless).

## Getting Started

Automation Testing environments management is not easy

### Prerequisites

Install [Docker](https://www.docker.com/) on your nodes.

Make sure [curl](https://en.wikipedia.org/wiki/CURL) works on your nodes (most unix-like system already contain it).

## Deployment

Deploy with below command:

**_startInstance.sh \<port\>_** [source](https://gitlab.com/huangjien_automationtest/instance/blob/master/startInstance.sh)

This command will pull an docker image from docker hub, the start a docker container, listening on the specified port.

## Running the tests

Download 2 files: [test.sh](https://gitlab.com/huangjien_automationtest/instance/blob/master/test.sh) and [testData.json](https://gitlab.com/huangjien_automationtest/instance/blob/master/testData.json).
Run **_test.sh \<port\>_** , if the return message does not show ***Fail***, then the deployment is correct.

## Built With

* [Spring](http://spring.io/projects/spring-boot) - The web framework used.
* [Gradle](https://gradle.org/) - Dependency Management.
* [Docker](https://www.docker.com/) - Used to deploy the instance.
* [Selenium](https://www.seleniumhq.org/) - Used to end to end test.

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/huangjien_automationtest/instance/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [versions of this project](https://gitlab.com/huangjien_automationtest/instance/tags). 

## Authors

* **Jien Huang** - *Initial work* - [Contact him](mailto:huangjien@gmail.com)

## License

This project is licensed under the MIT License.
Actually, I don't really know what MIT License is. So just use it, only provide limited support, no guarantee that the code is 100% correct. 

## Acknowledgments

* Thanks to everyone that reach this part, you are my real friends :)
* Inspiration by ??? May be every thing I saw since born to this world.