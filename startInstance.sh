if [ -z "$1" ]
then
    echo "Usage: startInstance.sh <listening port number>"
    exit 0
fi

docker pull huangjien/instance:latest
docker rm $(docker stop $(docker ps -a -q --filter name=automation-test-instance --format="{{.ID}}")) || true
docker run --name automation-test-instance -d -i -t -p 80:80 -p 443:443 -p $1:8090 automation-test/instance:latest