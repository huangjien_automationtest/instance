FROM selenium/standalone-chrome:latest

LABEL author=huangjien
USER seluser

ENV headless=true

ADD build/libs/instance.jar ./instance.jar
ADD config.properties .
ADD log4j.properties .
ADD my.properties .

EXPOSE 8090
EXPOSE 80
EXPOSE 443
EXPOSE 8443
EXPOSE 4444

ENTRYPOINT java -jar instance.jar com.automationtest.instance.AutoServer