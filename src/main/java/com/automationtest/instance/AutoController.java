package com.automationtest.instance;

import com.automationtest.base.Config;
import com.automationtest.base.Constants;
import com.automationtest.base.Utils;
import com.automationtest.instance.actions.ActionsFacade;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Properties;

@RestController
@CrossOrigin
public class AutoController {
  private Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private final JsonParser parser = new JsonParser();
  private Logger logger = LoggerFactory.getLogger("AutoServer");
  private final ActionsFacade facade = ActionsFacade.init();

  @RequestMapping(value = "/ping", method = RequestMethod.GET)
  @ResponseBody
  public String ping(){
    JsonObject json = new JsonObject();
    json.addProperty(Constants.STATUS, Constants.OK);
    return json.toString();
  }

  @RequestMapping(value = "/configure", method = RequestMethod.GET)
  @ResponseBody
  public String configure(){
    JsonObject json = new JsonObject();
    Properties properties = Config.getInstance().getAll();
    for (Object key : properties.keySet()) {
      json.addProperty(key.toString(), properties.get(key).toString());
    }
    json.addProperty(Constants.STATUS, Constants.OK);
    return json.toString();
  }

  @RequestMapping(value = "/configure", method = RequestMethod.POST)
  @ResponseBody
  public String configure(@RequestBody String propsString) {

    JsonElement props = parser.parse(propsString);
    for (String key : props.getAsJsonObject().keySet()) {
      String value = props.getAsJsonObject().get(key).getAsString();
      Config.getInstance().set(key, value);
    }
    JsonObject json = new JsonObject();
    json.add("properties", gson.toJsonTree(Config.getInstance().getAll()));
    json.addProperty(Constants.STATUS, Constants.OK);
    return json.toString();
  }

  @RequestMapping(value = "/test", method = RequestMethod.POST)
  @ResponseBody
  public String test(@RequestBody String cmdString) {
    JsonObject results = new JsonObject();
    results.addProperty(Constants.START_AT, Utils.now());
    JsonElement commands = parser.parse(cmdString);
    if(commands.isJsonArray()){
      JsonArray retArray = new JsonArray();
      for (JsonElement command : commands.getAsJsonArray()) {
        // TODO add return result
        JsonElement ret = facade.exec(command);
        ret.getAsJsonObject().add("command", command);
        retArray.add(ret);
      }
      results.add("result", retArray);
    } else {
      JsonElement ret = facade.exec(commands);
      ret.getAsJsonObject().add("command", commands);
      results.add("result", ret);
    }
    results.addProperty(Constants.END_AT, Utils.now());
    results.addProperty(Constants.STATUS, Constants.OK);
    return results.toString();
  }

}
