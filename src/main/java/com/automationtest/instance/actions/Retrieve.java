package com.automationtest.instance.actions;

import com.google.gson.JsonElement;
import org.openqa.selenium.WebElement;

public class Retrieve extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) throws Exception {
    if (invalid(jsonMessage, "Retrieve"))
      return defaultWrongActionNameResult();
    WebElement testObject = findOneTestObject(jsonMessage);
    if (testObject == null) {
      return defaultFailResult("Cannot find the test object:" + jsonMessage.toString());
    }
    JsonElement result = defaultOKResult();
    result.getAsJsonObject().add("found", getTestObjectJsonInfo(testObject));
    return result;
  }
}
