package com.automationtest.instance.actions;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import org.openqa.selenium.WebElement;

public class Check extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "Check"))
      return defaultWrongActionNameResult();
    WebElement testObject = findOneTestObject(jsonMessage);
    if (testObject == null) {
      return defaultFailResult("Cannot find the test object:" + jsonMessage.toString());
    }
    String data = getData(jsonMessage);
    assert testObject != null;
    boolean toBe = false;
    if (Strings.isNullOrEmpty(data) || data.equalsIgnoreCase("true") || data.equalsIgnoreCase("yes") || data.equalsIgnoreCase("y") || data.equalsIgnoreCase("on"))
      toBe = true;
    if (testObject.isSelected() != toBe)
      testObject.click();

    return defaultOKResult();
  }
}
