package com.automationtest.instance.actions;

import com.google.gson.JsonElement;

public class StartBrowser extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "StartBrowser"))
      return defaultWrongActionNameResult();
    BrowserInstance.getInstance().start();
    return defaultOKResult();
  }

}
