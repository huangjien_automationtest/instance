package com.automationtest.instance.actions;

import com.automationtest.base.Config;
import com.google.gson.JsonElement;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class ActionsFacade {
  private final HashMap<String, Action> actionMap = new HashMap<>();

  public static ActionsFacade init() {
    ActionsFacade facade = new ActionsFacade();
    String[] actions = Config.getInstance().get("actions").toString().split(";");
    for (String actionName : actions) {
      try {
        Class <? extends Action> clazz = Class.forName(actionName).asSubclass(Action.class);
        Action action = clazz.getDeclaredConstructor().newInstance();
        facade.actionMap.put(actionName, action);

      } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
        e.printStackTrace();
      }
    }
    return facade;
  }

  public JsonElement exec(JsonElement message){
    String actionName = Action.getActionName(message);
    Action action = getAction(actionName);
    assert action!=null;
    return action.exec(message);
  }

  private Action getAction(String actionName){
    for (String fullName : this.actionMap.keySet()) {
      if(fullName.contains(actionName)){
        return this.actionMap.get(fullName);
      }
    }
    return null;
  }
}
