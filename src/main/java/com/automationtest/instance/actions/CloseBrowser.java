package com.automationtest.instance.actions;

import com.google.gson.JsonElement;

public class CloseBrowser extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "CloseBrowser"))
      return defaultWrongActionNameResult();
    BrowserInstance.getInstance().close();
    return defaultOKResult();
  }
}
