package com.automationtest.instance.actions;

import com.automationtest.base.Config;
import com.automationtest.base.Constants;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Action {
  final Gson gson = new Gson();
  final JsonParser parser = new JsonParser();
  Logger logger = LoggerFactory.getLogger("Action");
  final long timeOut = Long.parseLong(Config.getInstance().get("search.timeout", "5").toString());
  final boolean alwaysTakeSnapshot = Config.getInstance().is("strategy.snapshot.always", false);
  private String[] attributes = Config.getInstance().get("web.element.attributes", "id;value;name;type;class;href;src").toString().split(";");

  public WebElement findOneTestObject(JsonElement message) {
    return Browser.findTestObject(message.getAsJsonObject().get("ui").getAsJsonObject(), timeOut);
  }

  public static String getActionName(JsonElement message){
    return message.getAsJsonObject().get("action").getAsString();
  }

  String getData(JsonElement message){
    return message.getAsJsonObject().get("data").getAsString();
  }

  UI getUI(JsonElement message) {
    return gson.fromJson(message.getAsJsonObject().get("ui"), UI.class);
  }

  public JsonElement exec(JsonElement jsonElement){
    JsonElement result = defaultOKResult();
    try {
      result = handle(jsonElement);
      if(alwaysTakeSnapshot && canTakeSnapshot(jsonElement))
        result.getAsJsonObject().addProperty("snapshot", Browser.snapshot());
    }catch (Exception e) {
      result = parser.parse("{\"" + Constants.STATUS +"\": \""+Constants.FAIL+"\" , \"Exception\":\"" + e.getMessage() + "\"}");
      if(canTakeSnapshot(jsonElement))
        result.getAsJsonObject().addProperty("snapshot", Browser.snapshot());
    }
    return result;
  }

  public JsonElement defaultOKResult() {
    return parser.parse("{\"" + Constants.STATUS + "\": \"" + Constants.OK + "\"}");
  }

  public JsonElement defaultWrongActionNameResult() {
    return defaultFailResult("Wrong Action Name");
  }

  public JsonElement defaultFailResult(String message) {
    return parser.parse("{\"" + Constants.STATUS + "\": \"" + Constants.FAIL + "\",  \"" + Constants.MESSAGE +"\": \""+message+"\"}");
  }

  private boolean canTakeSnapshot(JsonElement jsonElement){
    String noSnapshotActions = "CloseBrowser|Wait";
    String action = jsonElement.getAsJsonObject().get("action").getAsString();
    return !noSnapshotActions.contains(action);
  }

  public abstract JsonElement handle(JsonElement jsonMessage) throws Exception;

  public boolean invalid(JsonElement jsonMessage, String actionName) {
    return !getActionName(jsonMessage).equalsIgnoreCase(actionName);
  }

  public JsonElement getTestObjectJsonInfo(WebElement webElement) {
    assert webElement != null;
    JsonElement jsonElement = parser.parse("{}");
    for (String attribute : attributes) {
      String value = webElement.getAttribute(attribute);
      if (!Strings.isNullOrEmpty(value))
        jsonElement.getAsJsonObject().addProperty(attribute, value);
    }
    String tagName = webElement.getTagName();
    if (!Strings.isNullOrEmpty(tagName)){
      jsonElement.getAsJsonObject().addProperty("tagName", tagName);
      if(tagName.equalsIgnoreCase("select")){
        Select select = new Select(webElement);
        WebElement firstSelected = select.getFirstSelectedOption();
        if(firstSelected != null) {
          String text = firstSelected.getText();
          String value = firstSelected.getAttribute("value");
          jsonElement.getAsJsonObject().addProperty("selectedText", text);
          jsonElement.getAsJsonObject().addProperty("selectedValue", value);
        }
        JsonArray optionArray = new JsonArray();
        for (WebElement option : select.getOptions()) {
          optionArray.add(getTestObjectJsonInfo(option));
        }
        if(optionArray.size() > 0) {
          jsonElement.getAsJsonObject().add("options", optionArray);
        }
      }
    }
    String text = webElement.getText();
    if (!Strings.isNullOrEmpty(text)){
      jsonElement.getAsJsonObject().addProperty("linkText", text);
    }
    String checked = webElement.getAttribute("checked");
    if (!Strings.isNullOrEmpty(checked)){
      jsonElement.getAsJsonObject().addProperty("checked", checked);
    }else {
      jsonElement.getAsJsonObject().addProperty("checked", "");
    }

    return jsonElement;
  }
}
