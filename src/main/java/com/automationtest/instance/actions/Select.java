package com.automationtest.instance.actions;

import com.google.gson.JsonElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Select extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "Select"))
      return defaultWrongActionNameResult();
    WebElement testObject = findOneTestObject(jsonMessage);
    if (testObject == null) {
      return defaultFailResult("Cannot find the test object:" + jsonMessage.toString());
    }
    org.openqa.selenium.support.ui.Select select = new org.openqa.selenium.support.ui.Select(testObject);
    String data = getData(jsonMessage);
    try {
      select.selectByVisibleText(data);
    }catch (NoSuchElementException ex) {
      select.selectByValue(data);
    }
    return defaultOKResult();
  }
}
