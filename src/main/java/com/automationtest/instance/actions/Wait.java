package com.automationtest.instance.actions;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;

public class Wait extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) throws Exception {
    if (invalid(jsonMessage, "Wait"))
      return defaultWrongActionNameResult();
    String data = getData(jsonMessage);
    assert !Strings.isNullOrEmpty(data);
    long k = 1000;

    if (Strings.isNullOrEmpty(data)) {
      k = Long.parseLong(data) * k;

      Thread.sleep(k);

    } else {
      Thread.sleep(10 * k);
    }

    return defaultOKResult();

  }
}
