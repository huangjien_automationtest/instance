package com.automationtest.instance.actions;

import com.automationtest.base.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GetAll extends Action {
  private final String xpath = Config.getInstance().get("get.all","//input | //select | //a").toString();
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "GetAll"))
      return defaultWrongActionNameResult();
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("way", "xpath");
    jsonObject.addProperty("data", xpath);
    List<WebElement> allObjects = Browser.findTestObjects(jsonObject, 600);
    // generate json format return here:
    JsonElement result = defaultOKResult();
    JsonArray array = new JsonArray();
    for (WebElement webElement : allObjects) {
      array.add(getTestObjectJsonInfo(webElement));
    }
    result.getAsJsonObject().add("found", array);
    return result;
  }
}
