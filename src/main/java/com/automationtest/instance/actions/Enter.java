package com.automationtest.instance.actions;

import com.google.gson.JsonElement;
import org.openqa.selenium.WebElement;

public class Enter extends Action {
  @Override
  public JsonElement handle(JsonElement jsonMessage) {
    if (invalid(jsonMessage, "Enter"))
      return defaultWrongActionNameResult();
    WebElement testObject = findOneTestObject(jsonMessage);
    if (testObject == null) {
      return defaultFailResult("Cannot find the test object:" + jsonMessage.toString());
    }
    testObject.sendKeys(getData(jsonMessage));
    return defaultOKResult();
  }
}
