package com.automationtest.instance.actions;

import com.automationtest.base.Config;
import com.automationtest.base.Constants;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class BrowserInstance {
  private static final BrowserInstance _instance = new BrowserInstance();
  private WebDriver driver;
  final Logger logger = LoggerFactory.getLogger("Browser");

  private BrowserInstance() {

  }

  public static BrowserInstance getInstance() {
    return _instance;
  }

  public WebDriver get() {
    if (driver == null)
      start();
    return driver;
  }

  public void start() {
    if (Config.getInstance().get("browser.host", "local").toString().equalsIgnoreCase("sauce")) {
      startSauceBrowser();

    } else {
      startLocalBrowser();
    }
  }

  public String snapshot() {
    if (this.driver == null)
      return Constants.BLANK_IMAGE;
    try {
      return ((TakesScreenshot) this.driver).getScreenshotAs(OutputType.BASE64);
    } catch (Exception e) {
      // ignore exception:
      return Constants.BLANK_IMAGE;
    }
  }

  public void close() {
    if (driver == null) return;
    driver.quit();
  }


  private void startSauceBrowser() {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setBrowserName(Config.getInstance().get("sauce.browser", "firefox").toString());
    capabilities.setCapability("version", Config.getInstance().get("sauce.browser.version", "17"));

    capabilities.setCapability("platform", Platform.valueOf(Config.getInstance().get("sauce.os", "WINDOWS").toString()));
    capabilities.setCapability("name", Config.getInstance().get("sauce.test.name", "AutoX"));
    capabilities.setCapability("record-screenshots", true);
    capabilities.setCapability("public", Config.getInstance().get("sauce.public", "true").toString().equalsIgnoreCase("true"));
    try {
      driver = new RemoteWebDriver(
        new URL("http://" + Config.getInstance().get("sauce.user", "No User").toString()
          + ":" + Config.getInstance().get("sauce.key", "No key").toString()
          + "@ondemand.saucelabs.com:80/wd/hub"),
        capabilities);

    } catch (MalformedURLException e) {
      logger.error(e.getMessage(), e);
    }
    String url = Config.getInstance().get("test.url", "about:blank").toString();

    driver.get(url);
  }

  private void startLocalBrowser() {

    String browserName = Config.getInstance().get("local.browser", "firefox").toString();

    if (browserName.equalsIgnoreCase("chrome")) {
      getChromeDriver();

    }
    if (browserName.equalsIgnoreCase("firefox")) {
      getFireFoxDriver();
    }

    if (driver == null)
      throw new RuntimeException(String.format("Do not support this local browser: %s", browserName));
    String url = Config.getInstance().get("test.url", "about:blank").toString();
    driver.get(url);
  }

  private void getChromeDriver() {
    String currentPath = getCurrentPath();
    System.setProperty("webdriver.chrome.driver", currentPath + "/chromedriver");
    File driverFile = new File("./chromedriver");
    if (!driverFile.exists()) {
      System.setProperty("webdriver.chrome.driver",  "/usr/bin/chromedriver");
    }

    ChromeOptions options = new ChromeOptions();
    if (Config.getInstance().is("headless", false)) {
      options.setHeadless(true);
    }else {
      options.setHeadless(false);
    }
    options.addArguments("--window-size=1920x1080");

    try {
      driver = new ChromeDriver(options);
    }catch (Exception e) {
      e.printStackTrace();
      if (driver != null)
        driver.quit();
    }
  }

  private String getCurrentPath() {
    File currentDir = new File(".");
    String currentPath = currentDir.getAbsolutePath();
    if(currentPath.endsWith(".")){
      currentPath = currentPath.substring(0, currentPath.length() -1);
    }
    return currentPath;
  }

  private void getFireFoxDriver() {
    String currentPath = getCurrentPath();
    FirefoxBinary firefoxBinary = new FirefoxBinary();

    System.setProperty("webdriver.gecko.driver", currentPath + "geckodriver");


    FirefoxOptions firefoxOptions = new FirefoxOptions();
    File driverFile = new File("./geckodriver");
    if (!driverFile.exists()) {
      driverFile = new File("/usr/bin/geckodriver");
      System.setProperty("webdriver.gecko.driver",  "/usr/bin/geckodriver");
      firefoxOptions.setBinary("/usr/bin/firefox");
    }
    if (Config.getInstance().is("headless", false)) {
        firefoxOptions.setHeadless(true);
//        firefoxBinary.addCommandLineOptions("--headless");
    } else {
      firefoxOptions.setHeadless(false);
    }
    driver = new FirefoxDriver(firefoxOptions);
  }
}

